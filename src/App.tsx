import React, { useState } from "react";
import "./App.css";

import Header from "./components/Header";
import StopWatchTimer from "./components/StopWatchTimer";
import StartButton from "./components/StartButton";
import StopButton from "./components/StopButton";
import LapButton from "./components/LapButton";
import ResetButton from "./components/ResetButton";
import Laps from "./components/Laps";

type TimeObj = {
  hour: number;
  min: number;
  sec: number;
  msec: number;
};

type LapData = {
  num: number;
  hour: number;
  min: number;
  sec: number;
  msec: number;
  addedLapTime: TimeObj;
};

function App() {
  //stopwatch timer content
  const [time, setTime] = useState<TimeObj>({
    hour: 0,
    min: 0,
    sec: 0,
    msec: 0,
  });
  const [interv, setInterv] = useState<any>();
  const [num, setNum] = useState<number>(1);
  const [startState, setStartState] = useState<boolean>(false);
  const [stopState, setStopState] = useState<boolean>(false);
  const [laps, setLaps] = useState<LapData[]>([]);
  const [addedLapTime, setaddedLapTime] = useState<TimeObj>({
    hour: 0,
    min: 0,
    sec: 0,
    msec: 0,
  });

  //start button function
  const start = () => {
    run();
    setInterv(setInterval(run, 10));
    setStartState(true);
    setStopState(false);
  };

  //stop button function
  const stop = () => {
    clearInterval(interv);
    setStartState(false);
    setStopState(true);
  };

  //lap button function
  const lap = () => {
    setNum(num + 1);

    setaddedLapTime({
      hour: time.hour,
      min: time.min,
      sec: time.sec,
      msec: time.msec,
    });

    const lapData: {
      num: number;
      hour: number;
      min: number;
      sec: number;
      msec: number;
      addedLapTime: TimeObj;
    } = {
      num,
      hour: time.hour,
      min: time.min,
      sec: time.sec,
      msec: time.msec,
      addedLapTime,
    };

    setLaps((prevLaps) => {
      return [lapData, ...prevLaps];
    });
  };

  //reset button function
  const reset = () => {
    setTime({ hour: 0, min: 0, sec: 0, msec: 0 });
    setLaps([]);
    setNum(1);
    setStartState(false);
    setStopState(false);
    setaddedLapTime({ hour: 0, min: 0, sec: 0, msec: 0 });
  };

  //stopwatcgh timer logic

  let updateHour: number = time.hour,
    updateMin: number = time.min,
    updateSec: number = time.sec,
    updateMsec: number = time.msec;

  const run = () => {
    if (updateMin === 60) {
      updateHour++;
      updateMin = 0;
    }
    if (updateSec === 60) {
      updateMin++;
      updateSec = 0;
    }
    if (updateMsec === 100) {
      updateSec++;
      updateMsec = 0;
    }
    updateMsec++;
    return setTime({
      hour: updateHour,
      min: updateMin,
      sec: updateSec,
      msec: updateMsec,
    });
  };

  //maping laps
  let lapRecord: any = <p></p>;
  if (laps.length > 0) {
    lapRecord = laps.map((lap) => (
      <Laps
        key={lap.num}
        num={lap.num}
        hour={lap.hour}
        min={lap.min}
        sec={lap.sec}
        msec={lap.msec}
        addedLapTime={lap.addedLapTime}
      />
    ));
  }

  return (
    <React.Fragment>
      <Header />
      <StopWatchTimer time={time} />
      <div className="buttons">
        <StartButton
          start={start}
          startState={startState}
          //  stopState={stopState}
        />
        <StopButton
          stop={stop}
          startState={startState}
          //  stopState={stopState}
        />
        <LapButton
          lap={lap}
          startState={startState}
          //  stopState={stopState}
        />
        <ResetButton
          reset={reset}
          //  startState={startState}
          stopState={stopState}
        />
      </div>
      {lapRecord}
    </React.Fragment>
  );
}
export default App;
