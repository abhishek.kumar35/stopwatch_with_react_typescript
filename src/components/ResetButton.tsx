import React from "react";

const ResetButton: React.FC<{ stopState: boolean; reset: () => void }> = (
  props
) => {
  return (
    <React.Fragment>
      <button disabled={!props.stopState} onClick={props.reset}>
        Reset
      </button>
    </React.Fragment>
  );
};

export default ResetButton;
