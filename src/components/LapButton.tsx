import React from "react";

const LapButton: React.FC<{ lap: () => void; startState: boolean }> = (
  props
) => {
  return (
    <React.Fragment>
      <button disabled={!props.startState} onClick={props.lap}>
        Lap
      </button>
    </React.Fragment>
  );
};

export default LapButton;
